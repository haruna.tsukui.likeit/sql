

SELECT
category_name,
sum(item_price) as total_price
FROM item
INNER JOIN item_category
on item.category_id=item_category.category_id
group by category_name;
